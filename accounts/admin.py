# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from accounts.models import UserProfile

# Register your models here.

class UserProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'user_description')

    def user_description(self, obj):
        return obj.description

    



admin.site.register(UserProfile, UserProfileAdmin)
